package ex02;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;

public class TaskTwoMain {

	public static TaskTwoCustom generateObjects(ConcurrentSkipListMap<TaskTwoCustom, Integer> map) {
		Random rng = new Random();
		TaskTwoCustom custom = new TaskTwoCustom(rng.nextInt(1000000), rng.nextInt(1000000), rng.nextInt(1000000));
		return custom;
	}

	public static void fillMap(ConcurrentSkipListMap<TaskTwoCustom, Integer> map) {
		Random rng = new Random();
		int size = 6000;

		for (int i = 0; i < size; i++) {
			int value = rng.nextInt(size * 3);
			map.put(generateObjects(map), value);
		}
	}

	public static void readEvenEntries(ConcurrentSkipListMap<TaskTwoCustom, Integer> map) {
		Set<TaskTwoCustom> setEven = new HashSet<>();
		for (Entry<TaskTwoCustom, Integer> entry : map.entrySet()) {
			if (entry.getValue() % 2 == 0) {
				setEven.add(entry.getKey());
			}
		}
		System.out.println("Entries with even values: " + setEven.toString());
	}

	public static void readOddEntries(ConcurrentSkipListMap<TaskTwoCustom, Integer> map) {
		Set<TaskTwoCustom> setOdd = new HashSet<>();
		for (Entry<TaskTwoCustom, Integer> entry : map.entrySet()) {
			if (entry.getKey().getFieldInt() % 2 != 0) {
				setOdd.add(entry.getKey());
			}
		}
		System.out.println("Entries with odd keys: " + setOdd.toString());
	}

	public static void main(String[] args) {
		ConcurrentSkipListMap<TaskTwoCustom, Integer> skiplist = new ConcurrentSkipListMap<>();

		Runnable fill = () -> fillMap(skiplist);
		new Thread(fill).start();
		while (skiplist.size() < 6000) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		Runnable readEven = () -> readEvenEntries(skiplist);
		new Thread(readEven).start();

		Runnable readOdd = () -> readOddEntries(skiplist);
		new Thread(readOdd).start();
	}

}
