package ex02;

public class TaskTwoCustom implements Comparable<TaskTwoCustom> {
	private int fieldInt;
	private int secondFieldInt;
	private int thirdFieldInt;

	public TaskTwoCustom(int fieldInt, int secondFieldInt, int thirdFieldInt) {
		this.fieldInt = fieldInt;
		this.secondFieldInt = secondFieldInt;
		this.thirdFieldInt = thirdFieldInt;
	}

	public int getFieldInt() {
		return fieldInt;
	}

	public void setFieldInt(int fieldInt) {
		this.fieldInt = fieldInt;
	}

	public int getSecondFieldInt() {
		return secondFieldInt;
	}

	public void setSecondFieldInt(int secondFieldInt) {
		this.secondFieldInt = secondFieldInt;
	}

	public int getThirdFieldInt() {
		return thirdFieldInt;
	}

	public void setThirdFieldInt(int thirdFieldInt) {
		this.thirdFieldInt = thirdFieldInt;
	}

	@Override
	public int compareTo(TaskTwoCustom custom) {
		if (this.fieldInt > custom.fieldInt || this.secondFieldInt > custom.secondFieldInt
				|| this.thirdFieldInt > custom.thirdFieldInt) {
			return 1;
		}
		if (this.fieldInt < custom.fieldInt || this.secondFieldInt < custom.secondFieldInt
				|| this.thirdFieldInt < custom.thirdFieldInt) {
			return -1;
		}
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fieldInt;
		result = prime * result + secondFieldInt;
		result = prime * result + thirdFieldInt;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TaskTwoCustom))
			return false;
		TaskTwoCustom other = (TaskTwoCustom) obj;
		if (fieldInt != other.fieldInt)
			return false;
		if (secondFieldInt != other.secondFieldInt)
			return false;
		if (thirdFieldInt != other.thirdFieldInt)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[" + fieldInt + " " + secondFieldInt + " " + thirdFieldInt + "]";
	}
}
