package ex03;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.TreeMap;

public class TreeMapFiltering {
	public static ArrayList<String> filter(String line) {
		String[] words = line.toLowerCase().split(" ");
		ArrayList<String> list = new ArrayList<>(Arrays.asList());

		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].replace(".", "");
			words[i] = words[i].replace(",", "");
			words[i] = words[i].replace("!", "");
			words[i] = words[i].replace("?", "");
			words[i] = words[i].replace(":", "");
			words[i] = words[i].replace(";", "");
			words[i] = words[i].replace("`", "");
			words[i] = words[i].replace("'", "");
			words[i] = words[i].replace("\r\n", "");
			words[i] = words[i].replace("\n", "");
			words[i] = words[i].replace("\t", "");
			words[i] = words[i].replace("-", "");
		}

		for (String word : words) {
			list.add(word);
		}

		return list;
	}

	public static void main(String[] args) {
		Path path = Paths.get("sample_text.txt");
		ArrayList<String> wordsList = new ArrayList<>();
		TreeMap<String, Integer> words = new TreeMap<String, Integer>();

		try {
			BufferedReader reader = Files.newBufferedReader(path);
			String line = null;

			while ((line = reader.readLine()) != null) {
				wordsList.addAll(filter(line));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String word : wordsList) {

			if (words.containsKey(word)) {
				words.put(word, words.get(word) + 1);
			} else {
				words.put(word, 1);
			}
		}

		for (Entry<String, Integer> entry : words.entrySet()) {
			System.out.println(entry.getKey() + " - occurances: " + entry.getValue());
		}
	}

}
