package ex01;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class TreeMapVsHashMap {
	public static long mapFill(Map<Integer, Integer> map, int size) {
		Random rng = new Random();

		long startTimeHash = System.nanoTime();

		for (int i = 0; i < size; i++) {
			map.put(rng.nextInt(size * 2), rng.nextInt(size * 2));
		}

		return System.nanoTime() - startTimeHash;
	}

	public static void main(String[] args) {
		TreeMap<Integer, Integer> tMap = new TreeMap<>();
		HashMap<Integer, Integer> hMap = new HashMap<>();

		int size = 1000000;

		long timeT = mapFill(tMap, size);
		long timeH = mapFill(hMap, size);

		System.out.println("The time to map the TreeMap: " + (timeT / 1000000D) + " miliseconds.");
		System.out.println("The time to map the HashMap: " + (timeH / 1000000D) + " miliseconds.");

		if (timeH < timeT) {
			System.out.println("The mapping for HashMap is faster!");
		} else {
			System.out.println("The mapping for TreeMap is faster!");
		}
	}
}
